#include "rover_gazebo_plugins/WheelFeedback.h"
#include <memory>

using namespace gazebo;

void WheelFeedback::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
{
    this->model = _parent;
    this->sdf = _sdf;

    if (_parent->GetJointCount() == 0)
    {
        std::cerr << "Invalid joint count, Velodyne plugin not loaded\n";
        return;
    }

    this->joints_vector.push_back(_parent->GetJoint("base_to_left_1_wheel"));
    this->joints_vector.push_back(_parent->GetJoint("base_to_right_2_wheel"));
    this->joints_vector.push_back(_parent->GetJoint("base_to_left_3_wheel"));
    this->joints_vector.push_back(_parent->GetJoint("base_to_right_4_wheel"));
    this->joints_vector.push_back(_parent->GetJoint("base_to_left_5_wheel"));
    this->joints_vector.push_back(_parent->GetJoint("base_to_right_6_wheel"));

    InitializeROSMembers();

    this->updateConnection = event::Events::ConnectWorldUpdateBegin(std::bind(&WheelFeedback::OnUpdate, this));

    DebugMessage("Plugin is loaded");
}

void WheelFeedback::OnUpdate()
{
    ReadSimJointsData();
}

void WheelFeedback::InitializeROSMembers()
{
    if (!ros::isInitialized())
    {
        int argc = 0;
        char** argv = nullptr;
        ros::init(argc, argv, "wheel_feedback_plugin", ros::init_options::NoSigintHandler);
    }
    this->rosNode_unique_ptr = std::make_unique<ros::NodeHandle>(this->model->GetName());
    this->rosPubJointStates = this->rosNode_unique_ptr->advertise<universal_robot_msg::VelocityWheel>("/wheel_feedback", 10, false);
    this->timer = this->rosNode_unique_ptr->createTimer(ros::Duration(0.005), &WheelFeedback::timerCallback,
            this);  // TODO: Change rate later

    this->msg_joint_state = universal_robot_msg::VelocityWheel();

    this->num_joints = this->joints_vector.size();

    this->msg_joint_state.values.resize(num_joints);

    this->angVel.resize(num_joints);
    this->linVel.resize(num_joints);
    this->strAng.resize(num_joints);

    for (int i = 0; i < num_joints; i++)
    {
        this->msg_joint_state.values[i].ID = i + 1;
    }
}

void WheelFeedback::timerCallback(ros::TimerEvent const& event)
{
    for (int i = 0; i < num_joints; i++)
    {
        this->msg_joint_state.values[i].AngVelocity = this->angVel.at(i);
        // this->msg_joint_state.values[i].LinVelocity = this->linVel.at(i);
        // this->msg_joint_state.values[i].steerAngle = this->strAng.at(i);
    }
    this->rosPubJointStates.publish(this->msg_joint_state);
}

void WheelFeedback::ReadSimJointsData()
{
    for (int i = 0; i < num_joints; i++)
    {
        this->angVel.at(i) = GetJointAngularVelocity(joints_vector.at(i), 0u);
        this->linVel.at(i) = GetJointLinearVelocity(joints_vector.at(i), 0u);
        this->strAng.at(i) = GetJointPosition(joints_vector.at(i), 3);
    }
}

auto WheelFeedback::GetJointPosition(physics::JointPtr const joint, double const axis_index) -> double
{
#if GAZEBO_MAJOR_VERSION >= 8
    return 0.0;  // joint->Position(axis_index);
#else
    return joint->GetAngle(axis_index).Radian();
#endif
}

auto WheelFeedback::GetJointLinearVelocity(physics::JointPtr const joint, double const axis_index) -> double
{
    double vel = joint->GetVelocity(axis_index) * (this->wheel_radious);
    return vel;
}

auto WheelFeedback::GetJointAngularVelocity(physics::JointPtr const joint, double const axis_index) -> double
{
    return joint->GetVelocity(axis_index);
}
