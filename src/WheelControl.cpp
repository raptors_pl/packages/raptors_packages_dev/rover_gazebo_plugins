#include "rover_gazebo_plugins/WheelControl.h"



namespace gazebo{

    void WheelControl::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
    {
      // Safety check
      if (_model->GetJointCount() == 0)
      {
        DebugMessageERROR("Invalid joint count, Velodyne plugin not loaded\n");
        return;
      }

      // Store the model pointer for convenience.
      this->model = _model;

      this->setJointsParam();   

      this->InitializeROSMembers();

      // Spin up the queue helper thread.
      this->rosQueueThread = std::thread(std::bind(&WheelControl::QueueThread, this));

      DebugMessage("Plugin is loaded");

    }

    void WheelControl::InitializeROSMembers()
    {

        if(!ros::isInitialized()){
          int argc = 0;
          char **argv = NULL;
          ros::init(argc, argv, "gazebo_client", ros::init_options::NoSigintHandler);
        }

        this->rosNode.reset(new ros::NodeHandle("gazebo_client"));

        ros::SubscribeOptions so =
          ros::SubscribeOptions::create<universal_robot_msg::VelocityWheel>("/velocity",1,
          boost::bind(&WheelControl::OnRosMsg, this, _1),ros::VoidPtr(), &this->rosQueue);
          this->rosSub = this->rosNode->subscribe(so);

    }

    void WheelControl::OnRosMsg(const universal_robot_msg::VelocityWheel::ConstPtr &_msg)
    {
      for(int i = 0; i < this->joints_vector.size() ;i ++){
        this->SetVelocity(_msg->values[i].AngVelocity,i);
      }
    }

        void WheelControl::QueueThread()
    {
        static const double timeout = 0.01;
        while (this->rosNode->ok())
        {
            this->rosQueue.callAvailable(ros::WallDuration(timeout));
        }
    }

    void WheelControl::SetVelocity(const double &_vel, int wheel_num)
    { 
      this->model->GetJointController()->SetVelocityTarget(
        this->joints_vector.at(wheel_num)->GetScopedName(), _vel);
      //this->joints_vector.at(wheel_num)->SetVelocity(0,_vel);
    }

    void WheelControl::setJointsParam()
    {
      physics::Joint_V all_joints_vector = model->GetJoints();

      unsigned int i = 1;
      for(int w = 0; w < wheel_number; w++)
      {
        for(auto j : all_joints_vector)
        {

          std::string name = j->GetName();
          if(name.find(std::to_string(i)+"_wheel") != std::string::npos)
          {
            this->joints_vector.push_back(j);
            i++;
          }               
        }
      }
      
      
      // for(auto j : this->joints_vector)
      // {
      //   DebugMessage(j->GetScopedName());
      // }

      // Setup a P-controller, with a gain of 0.1.
      this->pid = common::PID(21, 3, 0);

      // Apply the P-controller to the joint.
      for(auto j : this->joints_vector)
      {
        this->model->GetJointController()->SetVelocityPID(j->GetScopedName(), this->pid);
      }
    }



    

GZ_REGISTER_MODEL_PLUGIN(WheelControl)
}