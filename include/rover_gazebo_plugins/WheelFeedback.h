#include <iostream>
#include <string>
#include <functional>

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include "universal_robot_msg/VelocityWheel.h"


#define BLUE_TXT1 "\e[1;34m"
#define NO_COLOR "\033[0m"

//https://github.com/yossioo/gazebo_ros_joints_publisher/blob/master/include/JointStatePublisher/JointStatePublisher.cpp

namespace gazebo
{
    class WheelFeedback: public ModelPlugin
    {
    public:
        void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);
        void OnUpdate();

    private:
        physics::ModelPtr model;
        physics::Joint_V joints_vector;
        sdf::ElementPtr sdf;
        event::ConnectionPtr updateConnection;

        //Define wheel radious
        float wheel_radious = 0.132;
        
        int num_joints = 0;
        std::vector<double> angVel, linVel, strAng;

        // ROS members
        std::unique_ptr<ros::NodeHandle> rosNode_unique_ptr;
        ros::Publisher rosPubJointStates, rosPubJointStates2;
        ros::Timer timer;
        universal_robot_msg::VelocityWheel msg_joint_state;

        void InitializeROSMembers();
        void timerCallback(const ros::TimerEvent &event);
        void ReadSimJointsData();

        double GetJointPosition(const physics::JointPtr joint, const double axis_index = 0);
        double GetJointLinearVelocity(const physics::JointPtr joint, const double axis_index);
        double GetJointAngularVelocity(const physics::JointPtr joint, const double axis_index);


        void DebugMessage(const std::string Message, const std::string color = BLUE_TXT1, const bool set_no_color = true) {
            std::string st = color + "[WheelFeedback] " +Message + NO_COLOR;
            ROS_INFO("%s", st.c_str());
        }

        void DebugMessageERROR(const std::string Message) {
            ROS_ERROR("%s", Message.c_str());
        }

    };

    // Register this plugin with the simulator
    GZ_REGISTER_MODEL_PLUGIN(WheelFeedback)
}

