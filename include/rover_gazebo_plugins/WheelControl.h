#include <sdf/sdf.hh>
#include <gazebo/gazebo.hh>                // for accessing all gazebo classes
#include <gazebo/common/common.hh>         // for common fn in gazebo like ModelPlugin
#include <gazebo/physics/physics.hh>       // for gazebo physics, to access -- ModelPtr
#include <gazebo/msgs/msgs.hh>
#include <ignition/math/Vector3.hh>        // to access Vector3d() from ignition math class

#include "ros/ros.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"

#include "universal_robot_msg/VelocityWheel.h"

#define BLUE_TXT1 "\e[1;34m"
#define NO_COLOR "\033[0m"

namespace gazebo
{
    class WheelControl : public ModelPlugin
    {

    public:
        void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);

    private:
        physics::ModelPtr model;
        physics::Joint_V joints_vector;                   
        const int wheel_number = 6; 
                  
        event::ConnectionPtr updateConnection;   
        common::PID pid;

        std::unique_ptr<ros::NodeHandle> rosNode;
        ros::Subscriber rosSub;
        ros::CallbackQueue rosQueue;
        std::thread rosQueueThread;
        physics::JointControllerPtr jointController; 

        void InitializeROSMembers();
        void OnRosMsg(const universal_robot_msg::VelocityWheel::ConstPtr &_msg);
        void QueueThread();

        void SetVelocity(const double &_vel, int wheel_num);
        void setJointsParam();

        void DebugMessage(const std::string Message, const std::string color = BLUE_TXT1, const bool set_no_color = true) {
            std::string st = color + "[WheelControl] " +Message + NO_COLOR;
            ROS_INFO("%s", st.c_str());
        }

        void DebugMessageERROR(const std::string Message) {
            ROS_ERROR("%s", Message.c_str());
        }

    };
};